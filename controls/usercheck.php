<?php
session_start();
?>
<?php
$email = $_POST['email'];
$pwd = $_POST['passwd'];

$servername = "localhost";
$usernamedb = "root";
$passworddb = "root";
$dbname = "picwars";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $usernamedb, $passworddb);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // prepare sql and bind parameters
    $stmt = $conn->prepare("SELECT * FROM t_utenti WHERE email = ?");
    $stmt->bindParam(1, $email);

    $stmt->execute();

    while ($row = $stmt->fetch()) {
        if (password_verify($pwd, $row['password'])){
            $_SESSION['usr'] = $row['username'];
            $_SESSION['iduser'] = $row['id_utenti'];
            $_SESSION['pic'] = $row['userpic'];
            $_SESSION['email'] = $row['email'];
            $_SESSION['year'] = $row['age_year'];
            $_SESSION['location'] = $row['residence'];
            $_SESSION['pwd'] = $row['password'];;
            header("Location: ../website/main");
            die();
        }
        header("Location: ../index?rsl=un");
        die();
    }
    header("Location: ../index?rsl=un");
    die();
}
catch(PDOException $e)
{
    header("Location: ../index?rsl=un");
}
$conn = null;
?> 