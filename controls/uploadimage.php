<?php
session_start();
?>
<?php
$target_dir = "../website/uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$finalmessage = "";
// Check file size
if ($_FILES["fileToUpload"]["size"] > 10485760) {
	$uploadOk = 0;
	$finalmessage = "Sorry, your file is either too large, or not is not an image.";
	header('Location: ../website/profile?rsl=errorimg1');
	die();
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
	$uploadOk = 0;
	$finalmessage = "Sorry, your file is either too large, or not is not an image.";
	header('Location: ../website/profile?rsl=errorimg2');
	die();
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
	$finalmessage = "Sorry, your file is either too large, or not is not an image.";
	header('Location: ../website/profile?rsl=errorimg3');
	die();

// if everything is ok, try to upload file
} else {
	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		insertimageindb($_FILES["fileToUpload"]["tmp_name"] . $target_file);
		$finalmessage = "Picture successfully uploaded!";
		header('Location: ../website/profile?rsl=succimg');
		die();
	} else {
		$finalmessage = "Sorry, your file is either too large, or not is not an image ". serialize($_FILES["fileToUpload"]);
		header('Location: ../website/profile?rsl=errorimg4');
		die();
	}
}

function insertimageindb(){
	$servername = "localhost";
	$usernamedb = "root";
	$passworddb = "root";
	$dbname = "picwars";
	$name = $_POST['title'];
	$description = $_POST['description'];
	$date = $_POST['date'];
	$place = $_POST['place'];
	$user = $_SESSION['iduser'];

	if($date == null){
		$date = CURDATE();
	}


	try {
		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $usernamedb, $passworddb);
    // set the PDO error mode to exception
		$sql =  $conn->prepare("INSERT INTO t_photos (name, description,time_date,place,who,filename) VALUES (?,?,?,?,?,?)");
		$sql->bindParam(1, $name , \PDO::PARAM_STR);
		$sql->bindParam(2, $description , \PDO::PARAM_STR);
		$sql->bindParam(3, $date , \PDO::PARAM_STR);
		$sql->bindParam(4, $place , \PDO::PARAM_STR);
		$sql->bindParam(5, $user , \PDO::PARAM_STR);
		$sql->bindParam(6, basename($_FILES["fileToUpload"]["name"]) , \PDO::PARAM_STR);
		$sql->execute();
		$conn = null;
		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $usernamedb, $passworddb);
    // set the PDO error mode to exception
		$sql =  $conn->prepare("UPDATE t_utenti SET last_upload = now() where id_utenti = $user");
		$sql->execute();

	}
	catch(PDOException $e)
	{
		echo "There was an error" . $e;
	}

	$conn = null;
}


?> 