<?php
session_start();
?>
<?php
$target_dir = "../website/uploads/users/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$finalmessage = "";
// Check file size
if ($_FILES["fileToUpload"]["size"] > 10485760) {
	$uploadOk = 0;
	$finalmessage = "Sorry, your file is either too large, or not is not an image.";
	header('Location: ../website/profile?rsl=errorimg1');
	die();
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
	$uploadOk = 0;
	$finalmessage = "Sorry, your file is either too large, or not is not an image.";
	header('Location: ../website/profile?rsl=errorimg2');
	die();
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
	$finalmessage = "Sorry, your file is either too large, or not is not an image.";
	header('Location: ../website/profile?rsl=errorimg3');
	die();

// if everything is ok, try to upload file
} else {
	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		insertimageindb($_FILES["fileToUpload"]["tmp_name"] . $target_file);
		$finalmessage = "Picture successfully uploaded!";
		header('Location: ../website/profile?rsl=succimg');
		die();
	} else {
		$finalmessage = "Sorry, your file is either too large, or not is not an image.";
		header('Location: ../website/profile?rsl=errorimg4');
		die();
	}
}

function insertimageindb(){
	$servername = "localhost";
	$usernamedb = "root";
	$passworddb = "root";
	$dbname = "picwars";
	$who = $_SESSION['iduser'];

	try {
		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $usernamedb, $passworddb);
    // set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$where = "../website/uploads/users/" . basename($_FILES["fileToUpload"]["name"]);
		$sql =  $conn->prepare("UPDATE t_utenti SET userpic = '$where' where id_utenti = '$who'");

		$sql->execute();

		header('Location: reloadaccount');
		die();

	}
	catch(PDOException $e)
	{
		echo $sql . "<br>" . $e->getMessage();
	}

	$conn = null;
}

?> 