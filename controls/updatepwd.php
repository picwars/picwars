<?php
session_start();
?>
<?php
$servername = "localhost";
$usernamedb = "root";
$passworddb = "root";
$dbname = "picwars";
$who = $_SESSION['iduser'];
$oldpwd = $_POST['oldpwd'];
$newpwd = $_POST['newpwd'];
$newpwd1 = $_POST['newpwd1'];
$pwd = $_SESSION['pwd'];

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $usernamedb, $passworddb);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if (password_verify($oldpwd, $pwd) == true && $newpwd == $newpwd1 && strlen($newpwd)>8 && strlen($newpwd)<20){
        $hashed = password_hash($newpwd, PASSWORD_DEFAULT);
        $sql =  $conn->prepare("UPDATE t_utenti SET password = ? where id_utenti = '$who'");
        $sql->bindParam(1, $hashed , \PDO::PARAM_STR);

        $sql->execute();

        header('Location: reloadaccount');
        die();
    }else{
        
        header('Location: reloadaccount');
        die();
    }
}
catch(PDOException $e)
{
    echo $sql . "<br>" . $e->getMessage();
}

$conn = null;
?> 