<?php

$servername = "localhost";
$usernamedb = "root";
$passworddb = "root";
$dbname = "picwars";
$name = $_POST['name'];
$surname = $_POST['surname'];
$email = $_POST['email'];
$email1 = $_POST['email1'];
$user = $_POST['username'];
$passwd = $_POST['passwd'];
$passwd1 = $_POST['passwd1'];
$hash = "";

if ($email != $email1 || $passwd != $passwd1){
    header('Location: register?rsl=un');
    die();
}else if(filter_var($email, FILTER_VALIDATE_EMAIL) == false){
    header('Location: register?rsl=unem');
    die();
}else if(strlen($passwd)<8 || strlen($passwd)>20){
    header('Location: register?rsl=unpwd');
    die();
}else{
    $hash = password_hash($passwd, PASSWORD_DEFAULT);
}
    
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $usernamedb, $passworddb);
    // set the PDO error mode to exception
    $sql =  $conn->prepare("INSERT INTO t_utenti (email, password,username, name, surname) VALUES (?,?,?,?,?)");
        $sql->bindParam(1, $email , \PDO::PARAM_STR);
        $sql->bindParam(2, $hash , \PDO::PARAM_STR);
        $sql->bindParam(3, $user , \PDO::PARAM_STR);
        $sql->bindParam(4, $name , \PDO::PARAM_STR);
        $sql->bindParam(5, $surname , \PDO::PARAM_STR);

        $sql->execute();

        header('Location: ../index?rsl=succ');
        die();
    }
    catch(PDOException $e)
    {
        echo "There was an error" . $e;
    }

    $conn = null;
    ?> 