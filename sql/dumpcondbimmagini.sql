-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: picwars
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_photos`
--

DROP TABLE IF EXISTS `t_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_photos` (
  `id_photos` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(300) NOT NULL,
  `time_date` date NOT NULL,
  `place` varchar(45) DEFAULT NULL,
  `who` int(11) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `time_upload` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_photos`),
  KEY `photo to id_idx` (`who`),
  CONSTRAINT `photo_to_id` FOREIGN KEY (`who`) REFERENCES `t_utenti` (`id_utenti`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_photos`
--

LOCK TABLES `t_photos` WRITE;
/*!40000 ALTER TABLE `t_photos` DISABLE KEYS */;
INSERT INTO `t_photos` VALUES (7,'Me','Me a calcetto','2015-07-07','Bollengo',5,'p4.jpg','2015-08-04 10:02:03'),(10,'El Crispi','Crispi stanco','2015-08-04','Rocket',5,'10940586_4964506007962_6247704886801698426_n.jpg','2015-08-04 10:02:03'),(11,'Me','Me','2015-08-04','Montagna',6,'altAtHrQQ1aLniXlhLzWXyUckcbxUzL17FvfOGqlDJv1Nw9.jpg','2015-08-04 10:02:03'),(12,'Cicci','cicci','2015-08-04','QUi',5,'altAjTqSdpGFf2of1JJDcumaVGX63wuRShR7gEeNd-7_vvF.jpg','2015-08-04 10:02:03'),(13,'Cs','CS','2015-08-04','Home',7,'Counter-strike-1.6-630x343.jpg','2015-08-04 10:02:03'),(14,'ll','ll','2015-08-04','ll',7,'profile_635845962_75sq_1383039407.jpg','2015-08-04 10:03:47'),(15,'ni','ni','2015-08-04','n',5,'p10.jpg','2015-08-04 10:29:15'),(16,'sad','sad','2015-08-04','sad',5,'p6.jpg','2015-08-04 10:36:39'),(17,'sdasda','sad','2015-08-04','2',5,'p9.jpg','2015-08-04 10:43:09'),(18,'sad','saddsa','2015-08-04','asd',5,'fotoprofilo1.png','2015-08-04 10:44:14'),(19,'sad','f','2015-08-04','2',6,'p1.jpg','2015-08-04 10:46:04');
/*!40000 ALTER TABLE `t_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_utenti`
--

DROP TABLE IF EXISTS `t_utenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_utenti` (
  `id_utenti` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `username` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `registrationdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `age_year` int(11) DEFAULT NULL,
  `residence` varchar(45) DEFAULT NULL,
  `userpic` varchar(100) DEFAULT '../img/default.jpg',
  `last_upload` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_utenti`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_utenti`
--

LOCK TABLES `t_utenti` WRITE;
/*!40000 ALTER TABLE `t_utenti` DISABLE KEYS */;
INSERT INTO `t_utenti` VALUES (5,'stefanogandiglio@hotmail.it','$2y$10$xNaDFArijW2kbKOo1pS63.hgcOi52T3hEuEWsv9TAvj0PFszPqelG','risikoruk','Stefano','Gandiglio','2015-07-29 22:00:00',NULL,NULL,'../img/default.jpg','2015-08-04 10:44:14'),(6,'gigi@gigi.it','$2y$10$TyYkTiqkjkwNIxquqgVvsuKcd2tFB2e/NlsNcRo5vrQ2opTZHuzhe','gigi','Gigi','Gigi','2015-08-03 22:00:00',NULL,NULL,'../img/default.jpg','2015-08-04 10:46:04'),(7,'we@we.it','$2y$10$HO4pvVgd.1UqVOioEqKi7OzEgl5Fy3PWGx7FLX7u5Gn6CuGsOozZy','we','we','we','2015-08-03 22:00:00',NULL,NULL,'../img/default.jpg','2015-08-04 10:35:05');
/*!40000 ALTER TABLE `t_utenti` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-04 16:33:56
