 <?php
 session_start();
 ?>
 <div class="post">
 	<?php
 	$servername = "localhost";
 	$usernamedb = "root";
 	$passworddb = "root";
 	$dbname = "picwars";
 	$who = $_SESSION['iduser'];
 	try {
 		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $usernamedb, $passworddb);
    // set the PDO error mode to exception
 		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // prepare sql and bind parameters
 		$stmt = $conn->prepare("SELECT *, userpic FROM t_photos INNER JOIN t_utenti ON t_photos.who = t_utenti.id_utenti WHERE who = '$who' order by who");
 		
 		$stmt->execute();
 		$newsub = true;
 		$html = "";
 		while ($row = $stmt->fetch()) {
 			if ($newsub == true){
 				$currentsub = $row['userpic'];
 				$html .= "<div class='profile-user-picture' style='background-image: url($currentsub)'></div>";
 				$newsub = false;
 			}
 			$location = "uploads/" .$row['filename'];
 			$html .=  "<div class='photo' style='background-image: url($location)'></div>";
 			if ($row['userpic'] != $currentsub){
 				$newsub = true;
 			}
 		}
 		echo $html;
 	}catch(PDOException $e){
 	}
 	$conn = null;
 	?>
 </div>