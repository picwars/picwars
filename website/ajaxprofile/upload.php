<!DOCTYPE html>
<html>
<body>
<form action="../../controls/uploadimage" method="post" enctype="multipart/form-data">
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="text" class="form-control upl"name="title" placeholder="Title">
    <input type="text" class="form-control upl" name="description" placeholder="Short description">
    <input type="date" class="form-control upl" name="date" placeholder="When was this taken? YYYY-MM-DD">
    <input type="text" class="form-control upl" name="place" placeholder="Where was this taken?">
    <input type="submit" class="btn btn-default" value="Upload Image" name="submit">
</form>
</body>
</html> 