<?php
session_start();
?>
<?php

if (!empty($_GET['rsl']) && $_GET['rsl'] == "em") {
  $result = "Email updated successfully.";
}else if (!empty($_GET['rsl']) && $_GET['rsl'] == "pwd") {
    $result = "Password updated succesfully";
}else if (!empty($_GET['rsl']) && $_GET['rsl'] == "year") {
    $result = "Password is too short or too long (Between 8 and 20).";
}else if (!empty($_GET['rsl']) && $_GET['rsl'] == "place") {
    $result = "Password is too short or too long (Between 8 and 20).";
}else if (!empty($_GET['rsl']) && $_GET['rsl'] == "unpwd") {
    $result = "Password is too short or too long (Between 8 and 20).";
}else if (!empty($_GET['rsl']) && $_GET['rsl'] == "unyear") {
    $result = "Inserted year is not valid.";
}

?>
<!DOCTYPE html>
<html>
<body>
    <form action="../../controls/updateemail" method="post">
     <h4>Current Email : <b><?php echo $_SESSION['email'];?></b><h4>
        <input type="text" class="form-control upl" name="email" placeholder="New Email">
        <input type="submit" class="btn btn-default" value="Submit" name="submit">
    </form>
    <form action="../../controls/updatepwd" method="post">
        <h4>Insert the old password in order to update it.<h4>
            <input type="password" class="form-control upl" name="oldpwd" placeholder="Old Password">
            <input type="password" class="form-control upl" name="newpwd" placeholder="New Password">
            <input type="password" class="form-control upl" name="newpwd1" placeholder="Repeat Password">
            <input type="submit" class="btn btn-default" value="Submit" name="submit">
        </form>
        <form action="../../controls/updateyear" method="post">
            <h4>Current year of birth set: <b><?php if ($_SESSION['year']==null){echo "Not yet set.";}else{echo $_SESSION['year']. ".";}?></b><h4>
                <input type="int" class="form-control upl" name="year" placeholder="Year of birth">
                <input type="submit" class="btn btn-default" value="Submit" name="submit">
            </form>
            <form action="../../controls/updateplace" method="post">
                <h4>Current location of birth set: <b><?php if ($_SESSION['location']==null){echo "Not yet set.";}else{echo $_SESSION['location']. ".";}?></b><h4>
                    <input type="text" class="form-control upl" name="place" placeholder="Where do you live?">
                    <input type="submit" class="btn btn-default" value="Submit" name="submit">
                </form>
                <form action="../../controls/updatepic" method="post" enctype="multipart/form-data">
                    <h4>Upload a new profile picture. It should be in either jpg or png format.<h4>
                        <input type="file" name="fileToUpload" id="fileToUpload">
                        <input type="submit" class="btn btn-default" value="Upload Image" name="submit">
                    </form>
                </body>
                </html> 