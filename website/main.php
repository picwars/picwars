<?php
session_start();
?>
<?php
if (!isset($_SESSION['usr'])){
  header("Location: ../index?rsl=unlo");
  die();
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>PicWars</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap -->
  <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
  <!-- Stylesheet -->
  <link href="../css/style.css" rel="stylesheet" media="screen">
  <!-- Script -->
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
  <script language="JavaScript" type="text/JavaScript" src="../js/mainscript.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <div id="header">
	  
        <div id="menutendina">
			<div id="logo" onclick="checkmenuhome()"></div>
				<ul id="tendina">
					<li><a href="#">Tournament</a></li>
					<li><a href="#">Winners</a></li>
				</ul>
		</div>
		
		
        <input type="text" id="navibar"/>
		
		<div id="menutendina2">
			<div onclick="menuprofile();" id="profilo" id="profpic" style="background-image: url('<?php echo $_SESSION['pic'];?>');" title='<?php echo $_SESSION['usr'];?>'></div>
				<ul id="tendina2">
					<a href="profile"><li>Profile</li></a>
					<a href="#"><li style="border-top:1px rgba(0,0,0,.1) solid; border-bottom:1px rgba(0,0,0,.1) solid;">Logout</li></a>
					<a href="#"><li>Segnala problemi</li></a>
				</ul>
		</div>
		
		<div class="icons" style="background-image: url(../img/win-ico.png)"></div>
		<div class="icons" style="background-image: url(../img/war-ico.png)"></div>
		
		
      </div>
      <?php
        $servername = "localhost";
        $usernamedb = "root";
        $passworddb = "root";
        $dbname = "picwars";
        $who = $_SESSION['iduser'];
          try {
           $conn = new PDO("mysql:host=$servername;dbname=$dbname", $usernamedb, $passworddb);
            // set the PDO error mode to exception
           $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

          // prepare sql and bind parameters
           $stmt = $conn->prepare("SELECT * FROM t_photos INNER JOIN t_utenti ON t_photos.who = t_utenti.id_utenti order by last_upload desc, who desc ,time_upload desc limit 20");
    
           $stmt->execute();
           $newsub = true;
           $html = "";
           $varcounter = 0;
           while ($row = $stmt->fetch()) {
            if (isset($currentsub) && $row['who'] != $currentsub){
              $newsub = true;
              $html .= "</div>";
            }
           if($newsub == true){
              $currentsub = $row['who'];
              $currentsubpic = $row['userpic'];
              $html .= "<div class='post'><div class='profile-user-picture' style='background-image: url($currentsubpic)'></div>";
              $newsub = false;
              $varcounter = 0;
            }
            if ($varcounter<5){
            $location = "uploads/" .$row['filename'];
            $html .=  "<div class='photo' style='background-image: url($location)'></div>";
            }
            $varcounter++;
           }
          echo $html;
        }catch(PDOException $e){
        }
        $conn = null;
        ?>
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="//code.jquery.com/jquery.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="../js/bootstrap.min.js"></script>
    </body>
    </html>