<?php
session_start();
?>
<?php
if (!isset($_SESSION['usr'])){
  header("Location: ../index?rsl=unlo");
  die();
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>PicWars</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap -->
  <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="../css/style.css" rel="stylesheet" media="screen">
  <link href="../css/simple-sidebar.css" rel="stylesheet" media="screen">
  <script src="../js/ajaxprofile.js"></script>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
      <![endif]-->
  <style type="text/css"> 
     a:link{color:#FFFFFF}
     a:visited{color:#FFFFFF}
  </style>
</head>
    <body>
      <div id="header">
        <a href="main"><div id="logo"></div></a>
        <input type="text" id="navibar"/>
        <a href="profile"><div id="profilo" style="background-image: url('<?php echo $_SESSION['pic'];?>');" title='<?php echo $_SESSION['usr']?>'></div></a>
      </div>
      <!-- Sidebar -->
      <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
          <!--<li class="sidebar-brand">
         </li>-->
         <li>
           <a href="javascript:updatediv(1);"><span>My Photos</span></a>
         </li>
         <li>
           <a href="javascript:updatediv(2);"><span>Upload Photos</span></a>
        </li>
        <li>
           <a href="javascript:updatediv(3);"><span>Subscriptions</span></a>
        </li>
        <li>
           <a href="javascript:updatediv(4);"><span>Account Details</span></a>
        </li>
      </ul>
    </div>
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
          </div>
        </div>
      </div>
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="//code.jquery.com/jquery.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="../js/bootstrap.min.js"></script>
    </body>
    </html>